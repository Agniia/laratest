<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');

Route::get('about', 'PageController@about')->name('about');

Route::get('hello', function () {
    echo 'Hello World!';
});

Route::get('user/{id?}', 'PageController@getUser');

Route::get('posts/{post}/{comment}', function ($comment, $postId) {
    dump('$comment' . $comment);
    dump('$postId' . $postId);
});

Route::resource('users', 'UsersCsontroller');