<?php

namespace App\Http\Controllers;

use App\Page;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(Request $request)
    {
        if (request()->has('user_id')) {
            echo 'we have user_id';
        }

        return view('main.welcome');
    }

    public function about()
    {
        $pages = Page::find(1)->user->pages;
        //$page = Page::find(1);
        //$user = User::find($page->user_id);
        //$pages = Page::where('user_id', $user->id)->get();
        return view('about', compact('pages'));
    }

    public function getUser($id)
    {
        dd($id);
    }
}
